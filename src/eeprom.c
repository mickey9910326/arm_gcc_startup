/**
 * @file eeprom.c
 * @author LCY
 * @brief EEPROM 相關函式實作
 * @date 2020.04.24
 */

#include "eeprom.h"

// instructions
#define READ    0b0011
#define WRITE   0b0010
#define WRDI    0b0100
#define WREN    0b0110
#define RDSR    0b0101
#define WRSR    0b0001

// test cs pin
#define P0TMPCS 24

uint8_t instr;

void eep_init(void){
    eep_cs_pinset();
    // SSP0 power setting
    PCONP |= PCONP_SSP0;

    // SSP0 clock setting
    // PCLK_peripheral = CCLK/4
    PCLKSEL1 &= ~(0xC00);

    // SSP0 pin setting
    // MOSI : P1.24
    // MISO : P1.23
    // SCK  : P1.20
    // CS   : P3.25
    // (1) Set function to be SSP0.
    // (2) Pin has neither pull-up nor pull-down resistor enabled.
    PINSEL3  |= 0x3C300;
    PINMODE3 |= 0x28200;
    // (1) Set function to be GPIO for SSP0.
    // (2) Pin has an on-chip pull-up resistor enabled.
    PINSEL7 &= ~(0xC0000);
    PINMODE7 &= ~(0xC0000);

    // basic operation of the SSP controller.
    // Data Size Select : 8-bit transfer
    SSP0CR0 = 0b000111;

    // SSP0 Enable.
    SSP0CR1 = 0b10;

    // Software can write data to be transmitted to this register, and read data that has been received
    SSP0ICR = 3;

    // Prescaler divides the SSP peripheral clock SSP_PCLK
    SSP0CPSR = 30;
}

void eep_write(uint8_t *write_data, uint16_t addr, uint16_t len){
    uint8_t addr_8;

    // 1 Byte write enable
    eep_cs_enable();
    instr = WREN;
    ssp0_write(&instr, 1);
    eep_cs_disable();

    // 1 Byte write instruction
    eep_cs_enable();
    instr = WRITE;
    ssp0_write(&instr, 1);

    // 2 Bytes address    
    addr_8 = addr >> 8;
    ssp0_write(&addr_8, 1);
    addr_8 = addr & 0xFF;
    ssp0_write(&addr_8, 1);

    // date write
    ssp0_write(write_data, len);
    eep_cs_disable();
}

void eep_read(uint8_t *read_data, uint16_t addr, uint16_t len) {
    uint8_t addr_8;
    
    // 1 Byte read instruction
    eep_cs_enable();
    instr = READ;
    ssp0_write(&instr, 1);

    // 2 Bytes address    
    addr_8 = (addr >> 8);
    ssp0_write(&addr_8, 1);
    addr_8 = addr & 0xFF;
    ssp0_write(&addr_8, 1);

    // date read
    ssp0_read(read_data, len);
    eep_cs_disable();
}

void eep_cs_pinset(void) {
    // Set direction of P3.25 P3.26 to be output
    FIO3DIR3 |= 0b10;
    FIO3SET3 |= 0b10;
    FIO0DIR |= (1 << P0TMPCS);
    FIO0SET |= (1 << P0TMPCS);
}

void eep_cs_enable(void) {
    // Set direction of P3.25 P3.26 pin output is set to HIGH
    // 0 : unchange
    // 1 : LOW
    FIO3CLR3 |= 0b10;
    FIO0CLR |= (1 << P0TMPCS);
}

void eep_cs_disable(void) {
    // Set direction of P3.25 P3.26 pin output is set to HIGH
    // 0 : unchange
    // 1 : HIGH
    FIO3SET3 |= 0b10;
    FIO0SET |= (1 << P0TMPCS);
}