/**
 * @file flash.h
 * @author LCY
 * @brief FLASH 相關函式定義
 * @date 2020.04.28
 * 
 * 硬體相關 :
 *      (1) 介面       : SSP0 (P1.20, P1.23, P1.24)
 *      (2) 記憶體大小 : 16 M-bytes
 *      (3) CS 腳位    : P0.21 (P0.25) 
 *          由於 FLASH CS使用腳位並未拉出，方便 debug ，
 *          使用另一隻 IO 與 EEPROM IO 同時做動，使使用者能以邏輯分析儀觀察
 *  注意點 :
 *      (1) 寫入保護 :
 *          欲寫入時須先解除保護，在本寫入函式亦有實作
 *      (2) 操作間隔 :
 *          以讀寫區塊大小為區別，分別占用不同時間，使用時需手動 delay
 *          參考表: https://gitlab.com/MVMC-lab/arm_gcc_startup/-/wikis/FLASH
 *      (3) [flash 操作流程 : 在寫入前須先經過抹除]
 *          寫入 : 僅有 page program
 *          讀取 : 不限筆數，以 CS 腳位上拉為停止點
 *          抹除 : 本函式僅有實作抹除 4K 大小的記憶體
 *  測試程式 :
 *      (1) test_flash_manu.c
 *          FLASH device 相關參數讀取
 *      (2) test_flash_RW.c
 *          FLASH 讀、寫、抹除測試
 */

#ifndef FLASH_H
#define FLASH_H

#include "system.h"
#include "LPC1769.h"
#include "regdef.h"
#include "ssp.h"

// read status register
#define READ_STATUS_1   0x05
#define READ_STATUS_2   0x35
#define READ_STATUS_3   0x15

// write status register
#define WRITE_STATUS_1   0x01
#define WRITE_STATUS_2   0x31
#define WRITE_STATUS_3   0x11

/**
 * @brief flash 初始化函式
 * 
 */
void flash_init(void);

/**
 * @brief flash 寫入函式
 * 
 * @param write_data 寫入資料指標
 * @param addr 欲寫入 flash 記憶體位置 (3-bytes)
 * @param len 寫入資料長度 (1~256bytes)
 */
void flash_write(uint8_t *write_data, uint64_t addr, uint16_t len);

/**
 * @brief flash 讀取函式
 * 
 * @param read_data 讀取資料儲存位置指標
 * @param addr 欲讀取 flash 記憶體位置 (3-bytes)
 * @param len 寫入資料長度 (不限)
 */
void flash_read(uint8_t *read_data, uint64_t addr, uint16_t len);

/**
 * @brief flash 裝置資訊讀取
 * 
 * @param manu_info 讀取資料儲存位置指標
 */
void flash_read_manu(uint8_t *manu_info);

/**
 * @brief flash 全記憶體抹除
 * 
 */
void flash_chip_erase(void);

/**
 * @brief flash 4K-記憶體抹除
 * 
 * @param addr 欲抹除 flash 記憶體起始位置 (3-bytes)
 */
void flash_erase_4K(uint64_t addr);

/**
 * @brief flash 讀寫狀態偵測
 * 
 * Note: 
 *      規格書裡是說，在資料寫入或是抹除時這個暫存器的狀態可以做為參考，
 *      但實際操作後，發現結果不太一樣，讓這兩個旗標作為完成動作的參考依據，
 *      卻發現後續讀取或是寫入都是失敗 (讀取值為0)，我覺得不太準
 *      [ 所以基本要使用還是手動 delay 比較好，這函式沒啥屁用 ]
 *
 * @param command 讀取或寫入指定狀態暫存器(1~3)的指令
 * @return uint8_t 指定狀態暫存器(1~3)值
 */
uint8_t flash_status(uint8_t command);

/**
 * @brief flash cs 腳位設定
 * 
 */
void flash_cs_pinset(void);

/**
 * @brief flash cs 腳位下拉
 * 
 */
void flash_cs_enable(void);

/**
 * @brief flash cs 腳位上拉
 * 
 */
void flash_cs_disable(void);

#endif // FLASH_H