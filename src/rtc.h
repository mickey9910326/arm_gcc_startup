/**
 * @file rtc.h
 * @author LCY
 * @brief LPC1769 RTC 相關基礎功能及結構定義
 * @date 2020.04.17
 * 
 * 未規劃且評估目前不會使用到的功能:
 *      1. RTC_CALIBRATION : 校準相關功能
 *      2. RTC_GPREG : 使用 GPIO 
 *      3. RTC_AUXEN : 中斷旗標偵測，debug用
 */

#ifndef RTC_H
#define RTC_H

#include "system.h"
#include "LPC1769.h"
#include "regdef.h"
#include <stdio.h>

/**
 * @brief 時間結構
 * 
 */
typedef struct {
    uint8_t sec;
    uint8_t min;
    uint8_t hour;
    uint8_t day_of_month;
    uint8_t month;
    uint16_t year;
} time_str;

/**
 * @brief 初始化 rtc 起始時間
 * 
 * @param year 欲設定之年分
 * @param month 欲設定之月份
 * @param day_of_month 欲設定之月號數
 * @param hour 欲設定之小時(24hr)
 * @param min 欲設定之分
 * @param sec 欲設定之秒
 */
void rtc_init(uint16_t year, uint8_t month, uint8_t day_of_month, uint8_t hour, uint8_t min, uint8_t sec);

/**
 * @brief 設定 rtc 鬧鐘時間
 * 
 * @param year 欲設定之年分
 * @param month 欲設定之月份
 * @param day_of_month 欲設定之月號數
 * @param hour 欲設定之小時(24hr)
 * @param min 欲設定之分
 * @param sec 欲設定之秒
 */
void rtc_alarm_init(uint16_t year, uint8_t month, uint8_t day_of_month, uint8_t hour, uint8_t min, uint8_t sec);

/**
 * @brief 開啟時鐘
 * 
 */
void rtc_start(void);

/**
 * @brief 關閉時鐘
 * 
 */
void rtc_stop(void);

/**
 * @brief 印出目前時間
 * 
 */
void rtc_current_time(void);

/**
 * @brief 初始化計時中斷
 * 
 * 搭配使用 :
 *      中斷函式:
 *      RTC_IRQHandler
 *  
 *      NVIC set:
 *      __enable_irqn(RTC_IRQn)
 * 
 *      進入中斷需手動清除 flag:
 *      RTC_ILR :
 *      Interrupt Location Register
 *          Bit 0 : Counter Increment Interrupt block generated an interrupt.
 *          Bit 1 : alarm registers generated an interrupt.
 */
void rtc_irq_en(void);

/**
 * @brief 反初始化計時中斷
 * 
 */
void rtc_irq_disable(void);

#endif // RTC_H