/**
 * @file ssp.c
 * @author LCY
 * @brief SSP 通訊函式實作
 * @date 2020.05.04
 */

#include "ssp.h"

void ssp0_write(uint8_t *tx, uint16_t len) {
    while (len--)
    {
        while ((SSP0SR & (SSP_SR_BSY | SSP_SR_TNF)) != (SSP_SR_TNF))
            ;
        SSP0DR = *tx++;
        while (SSP0SR & SSP_SR_BSY)
            ;
    }
}

void ssp0_read(uint8_t *rx, uint16_t len) {
    while (len--)
    {
        SSP0DR = 0x00;
        while ((SSP0SR & (SSP_SR_BSY | SSP_SR_RNE)) != (SSP_SR_RNE))
            ;
        *rx++ = SSP0DR;
    }
}

void ssp1_write(uint8_t *tx, uint16_t len) {
    while (len--)
    {
        while ((SSP1SR & (SSP_SR_BSY | SSP_SR_TNF)) != (SSP_SR_TNF))
            ;
        SSP1DR = *tx++;
        while (SSP1SR & SSP_SR_BSY)
            ;
    }
}

void ssp1_read(uint8_t *rx, uint16_t len) {
    while (len--)
    {
        SSP1DR = 0x00;
        while ((SSP1SR & (SSP_SR_BSY | SSP_SR_RNE)) != (SSP_SR_RNE))
            ;
        *rx++ = SSP1DR;
    }
}