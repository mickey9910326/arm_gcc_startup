/**
 * @file test_eeprom.c
 * @author LCY
 * @brief LPC1769 EEPROM 讀寫測試
 * @date 2020.04.25
 */

#include "uart.h"
#include "system.h"
#include "eeprom.h"
#include "LPC1769.h"
#include <stdio.h>

int main(void) {
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);

    system_clock_init();
    system_power_init();
    pinconnect_init();

    uart3_init();
    eep_init();

    printf("---START---\n");

    // ********** I/O data initial **********
    uint16_t ADDR = 0x0000;
    uint8_t data[10] = {0};
    uint8_t recv_data[10] = {0};
    for (int i = 0; i < 10; i++) data[i] = i*9 + 1;
    
    // ********** EEPROM Operation **********
    // eep_write(data, ADDR, 5);
    
    for (uint32_t i = 0; i < 100000; i++)
        __asm("nop");

    eep_read(recv_data, ADDR, 5);

    return 0;
}