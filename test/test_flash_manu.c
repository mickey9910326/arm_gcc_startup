/**
 * @file test_flash_manu.c
 * @author LCY
 * @brief FLASH 裝置資訊測試
 * @date 2020.04.30
 * 
 * FLASH 裝置基本資訊，可預先使用此程式測試 FLASH 硬體
 */

#include "uart.h"
#include "system.h"
#include "flash.h"
#include "LPC1769.h"
#include <stdio.h>

int main(void)
{
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);

    system_clock_init();
    system_power_init();
    pinconnect_init();

    uart3_init();
    flash_init();

    printf("---START---\n");

    uint8_t manufac[2] = {0};
    flash_read_manu(manufac);

    return 0;
}