#include "uart.h"
#include "system.h"
#include <stdio.h>

static const char a[10] = "hello";
uint8_t ch;
float sd;
extern uint32_t __etext[];
extern uint32_t __data_start__[];

int main (void) {
    uint8_t data = 255;

    setbuf(stdout, NULL);
    setbuf(stdin, NULL);

    system_clock_init();
    system_power_init();
    pinconnect_init();

    uart3_init();
    uart3_putc(a[0]);
    uart3_putc(a[1]);
    uart3_putc(a[2]);
    uart3_putc(a[3]);
    uart3_putc(a[4]);
    uart3_putc('\n');

    printf("hello world\n");
    printf("__etext = %p\n", __etext);
    printf("__data_start__ = %p\n", __data_start__);

    // printf with args test
    printf("data is = %d\n", data);

    return 0;
}
